from django.test import TestCase
from django.urls import reverse, resolve
from ..views import home
from ..models import Throttle, User


class HomePageTest(TestCase):

    def setUp(self):
        url = reverse('common:home')
        self.response = self.client.get(path=url)

    def test_status_code(self):
        self.assertEquals(self.response.status_code, 200)

    def test_home_url_resolves_home_view(self):
        view = resolve('/')
        self.assertEquals(view.func, home)

    def test_home_authentication_page_contain_username(self):
        data = {
            'username': 'ali',
            'email': 'alirad@gmail.com',
            'password1': 'uq772ohn',
            'password2': 'uq772ohn'
        }
        url = reverse('account:signup')
        self.client.post(path=url, data=data)
        user = User.objects.all()[0]
        Throttle.objects.create(pattern=url, limit="10/sec", user=user)
        response = self.client.get(reverse('common:home'))
        self.assertContains(response, f'href="{url}"')
