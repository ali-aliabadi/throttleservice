from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter
@stringfilter
def contain_create(value):
    if value.__contains__("Create"):
        return 'Create'
    else:
        return 'Edit'
