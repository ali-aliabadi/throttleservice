import time

from django.test import TestCase
from django.urls import reverse

from common.models import Throttle, User


# Create your tests here.

class TestAccess(TestCase):

    def setUp(self):
        user = User.objects.create(username='ali', password="uq772ohn")
        Throttle.objects.create(pattern="example.com/foo/.*", limit='minute', number=5, user=user)

    def test_access_view_in_two_minute_13_request_posted(self):
        for i in range(12):
            if i == 6:
                time.sleep(60)
            data = {
                'pattern': "example.com/foo/bar",
                'ip': '192.168.1.1'
            }
            response = self.client.post(reverse('service:access'), data=data)
            if i in {0, 1, 2, 3, 4, 6, 7, 8, 9, 10}:
                self.assertEqual(response.status_code, 200)
            else:
                self.assertEqual(response.status_code, 429)
        time.sleep(60)

    def test_access_bad_request_post(self):
        data = {
            'pattern': "eeexample.com/foo/bar",
            'ip': '192.168.1.1'
        }
        response = self.client.post(reverse('service:access'), data=data)
        self.assertEqual(response.status_code, 400)

    def test_access_get_request_api_post(self):
        data = {
            'pattern': "example.com/foo/bar",
            'ip': '192.168.1.1'
        }
        response = self.client.get(reverse('service:access'), data=data)
        self.assertEqual(response.status_code, 400)
