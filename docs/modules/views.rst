Views
======

.. automodule:: account.views
   :members:
   :undoc-members:

.. automodule:: common.views
   :members:
   :undoc-members:

.. automodule:: service.views
   :members:
   :undoc-members:
