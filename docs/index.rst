.. Throttle Service documentation master file, created by
   sphinx-quickstart on Mon Mar 30 18:27:40 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Throttle Service's documentation!
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules/views.rst
   modules/models.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
