Throttle Service is a rate limiting handler api that you can run on your machine and use it for your website.

**pre-installation** :

    install docker
    install docker-compose

**installation with docker** :
in path your project that clone, open terminal and run command below
    
    sudo docker-compose up --build
    
then for attach database to your project run commands below:
for find out throttleservice_web's container id
    
    sudo docker ps -a
            
copy the throttleservice_web's container id and run command below:
    
    sudo docker exec -t -i YOUR_CONTAINER_ID bash

you are now in the docker environment you have to run commands below in this environment
    
    python manage.py makemigrations
    python manage.py migrate

you must create a user for use this app then run command below for create a user 

    python manage.py createsuperuser

enter your username / email / password

your user create then you can exit from environment docker with command: exit  

your django app running on localhost:8001/

open a browser and type localhost:8001/

**Usage** :
now you can save your end points with rate limiting setting in this app and 
send request of any your user that want to access to your endpoint and 
get a response form app if status code of response was 429 that means user hasn't access
if status code was 200 that means that user has access .  
data that you must send to ThrottleService api contains pattern and ip.
for example:

    import requests
    data = {
        "pattern":"example.com",
        "ip":"123.123.123.123"
    }
    response = requests.post('http://localhost:8001/service/access/', data=data)
    if response.status_code == 200:
        #this means user has access to your pattern
        pass
    elif response.status_code == 429:
        #this means user hasn't access to your pattern
        pass
    else:
        #this means you send bad request
        pass
    
you can get ipv-4 of your user with copy function that implemented in service/views.py/get_client_ip() in yor website.

**Documentation** :
for create html documentation you just go to docs/ directory and run command bellow on terminal
    
    make html
    
**Testing** : 
for testing project run commands below:

    sudo docker exec -t -i YOUR_CONTAINER_ID bash
    python manage.py test



  


