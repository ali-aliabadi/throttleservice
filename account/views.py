from django.shortcuts import render, redirect
from django.contrib.auth import login as auth_login
from account.forms import SignUpForm, ChangePassword
from django.contrib.auth.decorators import login_required
from django.contrib.auth import update_session_auth_hash


def signup(request):
    """
    Register user.

    **Description**

    Get request from user and create a User instance with form SignUpForm and save it in Users's model in database.

    **Template:**

    ThrottleService/templates/signup.html

    :param request:
    :return: if sign up successful redirect to home page else rendering sign up page for trying to sign up.
    """
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            auth_login(request, user)
            return redirect('common:home')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


@login_required
def change_password(request):
    """
    Updating a user's password.

    **Description**

    Take the current request and the updated user object from form ChangePassword and update user's password and
    save it in database.

    **Template:**

    ThrottleService/templates/changePassword.html

    :param request:
    :return: if successful redirect to home page else rendering change password page for trying to change password.
    """
    form = ChangePassword(user=request.user, data=request.POST or None)
    if form.is_valid():
        user = form.save()
        update_session_auth_hash(request, user)
        return redirect('common:home')
    return render(request, 'changePassword.html', {'form': form})
