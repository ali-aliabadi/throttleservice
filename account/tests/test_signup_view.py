from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import resolve, reverse
from account.views import signup
from common.models import Throttle
from ..forms import SignUpForm


class SignUpTest(TestCase):

    def setUp(self):
        url = reverse('account:signup')
        self.response = self.client.get(url)

    def test_signup_status_code(self):
        self.assertEquals(self.response.status_code, 200)

    def test_signup_url_resolves_signup_view(self):
        view = resolve('/account/signup/')
        self.assertEquals(view.func, signup)

    def test_csrf(self):
        self.assertContains(self.response, 'csrfmiddlewaretoken')

    def test_contains_form(self):
        form = self.response.context.get('form')
        self.assertIsInstance(form, SignUpForm)


class SuccessfulSignup(TestCase):
    def setUp(self):
        data = {
            'username': 'ali',
            'email': 'alirad@gmail.com',
            'password1': 'uq772ohn',
            'password2': 'uq772ohn'
        }
        self.url = reverse('account:signup')
        self.response = self.client.post(path=self.url, data=data)


    def test_successful_signup_status_code(self):
        self.assertEqual(self.response.status_code, 302)

    def test_redirect_to_home_page(self):
        self.assertRedirects(self.response, reverse('common:home'))

    def test_authentication_user(self):
        response = self.client.get(reverse('common:home'))
        user = response.context.get('user')
        self.assertTrue(user.is_authenticated)

    def test_create_user(self):
        self.assertTrue(User.objects.exists())


class InvalidSignUpTests(TestCase):
    def setUp(self):
        url = reverse('account:signup')
        self.response = self.client.post(url, {})  # submit an empty dictionary

    def test_signup_status_code(self):
        self.assertEquals(self.response.status_code, 200)

    def test_form_errors(self):
        form = self.response.context.get('form')
        self.assertTrue(form.errors)

    def test_dont_create_user(self):
        self.assertFalse(User.objects.exists())
